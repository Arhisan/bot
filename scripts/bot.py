# -*- coding: utf-8 -*-
__author__ = 'pisag'
import telebot
import config
import random
from telebot import types
import utilsMy

speech = ["Что-то фигня какая-то, а не пример...","Я не вижу здесь примера.",
          "Что это? Я за тебя думать не буду!","Проверь запись, пожалуйста","Я не понимаю такого стиля, можно что-то более классическое?",
          "На нормальный вопрос, будет нормальный ответ, а это вопрос не нормальный!", "Я не понимаю, что значит твое: "]
bot = telebot.TeleBot(config.token)

mode = 1
n1=0
n2=0
n3=0


@bot.message_handler(content_types=["text"])
def responce(message):
    global mode
    global n1
    global n2
    global n3
    keyboard1 = types.InlineKeyboardMarkup()
    keyboard2 = types.InlineKeyboardMarkup()
    keyboard1.add( types.InlineKeyboardButton(text="Тебе сюда! СРОЧНО!", url="http://mat-zadachi.ru/"))
    keyboard2.add( types.InlineKeyboardButton(text="Мда... Нажимай и учись...", url="http://mathematics-tests.com/matematika-3-klass-new/zadachi-primery/3-4-chetverty"))
    if(message.text=="Cпрашивать"):
        mode = 1
    if(message.text=="Решать"):
        mode = 2
        n3 = 9999999

    if mode == 1:
        
        # str2 = filter(lambda x: x in "0123456789+-*/()", message.text)
        #ЭКВИВАЛЕНТНО, но лучше по памяти
        str2=''
        for c in message.text:
            if c in ('0','1','2','3','4','5','6','7','8','9','+','-','*','/','(',')'):
                str2=str2+c
        #КОНЕЦ

        if (len(message.text)<=32):
            try:
                a = eval(str2)
            except:
                c = random.randint(0,len(speech)-1)
                a = speech[c]
                if c == len(speech)-1:
                    a += message.text
            print (a)
        else:
            a = "Неее, фигня какая-то, давай-ка чего попроще."
        bot.send_message(message.chat.id, a, reply_markup=utilsMy.generate_markup("Спрашивать","Решать"))
            
    elif mode == 2:
        if(n3==9999999):
             bot.send_message(message.chat.id, "В этом решиме решать будешь ты, а не я, вот пример", reply_markup=utilsMy.generate_markup("Спрашивать","Решать"))
        if(message.text==str(n3)):
            bot.send_message(message.chat.id, "Верно, следующий пример\n", reply_markup=utilsMy.generate_markup("Спрашивать","Решать"))
            print("+")
        elif n3!=9999999:
            if(random.randint(1,10)<2):
                key = keyboard1
            elif(random.randint(1,10)<3):
                key = keyboard2
            else:
                key = utilsMy.generate_markup("Спрашивать","Решать")
            bot.send_message(message.chat.id, "Нет! Ответ "+str(n3)+ ". Cледующий пример\n", reply_markup=key)
            print("-")
        n2 = random.randint(1,20)
        n3 = random.randint(1,20)
        op = random.randint(1,4)
        n1=0
        if op == 4:
            n1 = n3*n2
            a = str(n1)+"/"+str(n2)
        if op == 3:
            n1 = n3-n2
            a = str(n1)+"+"+str(n2)
        if op == 2:
            n1 = n3+n2
            a = str(n1)+"-"+str(n2)
        if op == 1:
            n1 = random.randint(1,20)
            n3 = n1*n2
            a = str(n1)+"*"+str(n2)
        print(a)
        bot.send_message(message.chat.id, a, reply_markup=utilsMy.generate_markup("Спрашивать","Решать"))

if __name__ == '__main__':
    bot.polling(none_stop=True)
