# -*- coding: utf-8 -*-
__author__ = 'pisag'
from telebot import types

mark = 0
question = 0

def generate_markup(right_answer, wrong_answers):
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=False, resize_keyboard=True)
    all_answers = '{},{}'.format(right_answer, wrong_answers)
    list_items = []
    for item in all_answers.split(','):
        list_items.append(item)
    # list_items += all_answers.split(',') # Эквивалентная запись, которая работает быстрее
    for item in list_items:
        markup.add(item)
    return markup

